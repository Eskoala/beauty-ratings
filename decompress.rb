#given a hash of filetype/decompressing program pairs, and an array of directories, it decompresses all the files that match a decompressor available to it.
def decompress(decompressors, directories)
	directories.each do |genre|
		puts "doing directory #{genre}"
		Dir.foreach(genre) do |track|
			puts "doing track #{track}"
			decompressors.each do |filetype, decompressor|
				if(track =~ filetype)
	 				system("#{decompressor} \"#{genre}/#{track}\"")			
	 			end
	 		end
 		end
 	end
end


	