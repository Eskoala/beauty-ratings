#instructions to tacet
DECOMPRESS = false
EXTRACT_FEATURES = true
LEARN = true


#music directories and their genres

GENREDIRS = {"ugly-all" => "ugly-all",
			"beauty-all" => "beauty-all"}

#GENREDIRS = {'testBach' => '/Users/esk/Desktop/CMmusic/testBach',
#			'testBeet' => '/Users/esk/Desktop/CMmusic/testBeet',
#			'testBra' => '/Users/esk/Desktop/CMmusic/testBra',
#			'testMoz' => '/Users/esk/Desktop/CMmusic/testMoz',
#			'testHay' => '/Users/esk/Desktop/CMmusic/testHay'}

#decompression program paths
#/\.flac$/ => "/opt/local/var/macports/sources/rsync.macports.org/release/ports/audio/flac -d ",
DECOMPHASH = {/\.mp3$/ => "/Applications/lame-398-2/frontend/lame --decode"}
			
#extraction program paths
COLLECTIONMAKER = "/usr/local/bin/mkcollection -c"
FEATUREEXTRACTOR = "/usr/local/bin/bextract"
LEARNING = "/usr/local/bin/kea"

WFILE = "def-all-20120913.arff"

#should auto get the genres
#EXTRACT_OPTIONS = "-sv -timbral -chroma -scf -sfm -lsp -lpcc ugly-all.mf beauty-all.mf -w #{WFILE}"
#EXTRACT_OPTIONS = "-sv -timbral -chroma ugly-all.mf beauty-all.mf -w #{WFILE}"
EXTRACT_OPTIONS = "-sv ugly-all.mf beauty-all.mf -w #{WFILE}"

LEARN_OPTIONS = "-od . -w"
#EXTRACT_OPTIONS = "-sv testBach.mf testBra.mf testBeet.mf testMoz.mf testHay.mf -w test.arff"
