require './config.rb'
require './decompress.rb'
#require renameWav.rb'
require './extractfeatures.rb'
require './runlearning.rb'

if (DECOMPRESS)
	decompress(DECOMPHASH,GENREDIRS.values)
	renameWav(GENREDIRS.values)
end
if(EXTRACT_FEATURES)
	make_collections(COLLECTIONMAKER,GENREDIRS)
	extract(FEATUREEXTRACTOR, EXTRACT_OPTIONS)
end
learn(LEARNING, LEARN_OPTIONS, WFILE) if LEARN
