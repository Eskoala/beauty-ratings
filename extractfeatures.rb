# mkcollection [-l label] [-c collectionName] dir1 dir2 ... dirN
# bextract [-e extractor] [-h help] [-s start(seconds)] [-l length(seconds)] [-m memory]  [-u usage] collection1 collection2 ... collectionN

def make_collections(collect_prog, genre_directories)
	genre_directories.each do |genre, dir|
		system("#{collect_prog} #{genre}.mf \"#{dir}\"")
	end
end
def extract(extract_prog, options)
	system("#{extract_prog} #{options}")
end